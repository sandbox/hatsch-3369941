<?php

namespace Drupal\ovenmedia\Controller;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Serialization\Json;
use Drupal\ovenmedia\Event\OvenMediaIncomingStreamAdmissionOpeningEvent;
use Drupal\ovenmedia\Event\OvenMediaIncomingStreamAdmissionClosingEvent;
use Drupal\ovenmedia\Event\OvenMediaOutoingStreamAdmissionOpeningEvent;
use Drupal\ovenmedia\Event\OvenMediaOutgoingStreamAdmissionClosingEvent;
use Drupal\Core\Datetime\DrupalDateTime;



/**
 * Returns responses for Oven Media Engine Admission Webhooks routes.
 */
class OvenMediaController extends ControllerBase {

  /**
   * Ovenmedia Admission Webhook handler.
   */
  public function postAdmission(Request $req) {
    $service = \Drupal::service('ovenmedia.service');

    $data = Json::decode($req->getContent());

    $src = $data['client']['address'];
    $client_port = $data['client']['port'];
    $direction = $data['request']['direction'];
    $protocol = $data['request']['protocol'];
    $url = $data['request']['url'];
    $time = $data['request']['time'];
    $status = $data['request']['status'];

    // Parse url.
    $stream = parse_url($url);
    $host = $stream['host'];
    $port = $stream['port'];

    $path = explode('/', $stream['path']);
    $incoming_app = $path[1];
    $incoming_stream = $path[2];

    parse_str($stream['query'], $query);
    $psk = $query['psk'];

    $accepted = FALSE;

    if ($direction == 'outgoing') {
      if ($status == 'opening') {
        $event = new OvenMediaOutgoingStreamAdmissionOpeningEvent($entity);
        $event_dispatcher = \Drupal::service('event_dispatcher');
        $event_dispatcher->dispatch($event, OvenMediaOutgoingStreamAdmissionOpeningEvent::OPENING_EVENT);        
      }
      if ($status == 'closing') {
        $event = new OvenMediaOutgoingStreamAdmissionOpeningEvent($entity);
        $event_dispatcher = \Drupal::service('event_dispatcher');
        $event_dispatcher->dispatch($event, OvenMediaOutgoingStreamAdmissionOpeningEvent::CLOSING_EVENT);           
      }

      // Todo: allow overriding somehow for custom permission logic.
      // For now just allow everyone watching the stream.
      $res['allowed'] = TRUE;
      $res['reason'] = 'authorized';
      return new JsonResponse($res);
    }

    if ($direction == 'incoming') {
      if ($result = $service->getFieldFromAppName($incoming_app)) {
        // Check if there is a matching stream name.
        $field = $result['field'];
        $entity_type = $result['entity_type'];
        $field_name = $field->getName();

        $ids = \Drupal::entityTypeManager()->getStorage($entity_type)->loadByProperties([$field_name . '.ome_stream_name' => $incoming_stream]);
        if (count($ids) == 1) {
          $entity = array_shift($ids);

          $stream_psk = $entity->{$field_name}->ome_stream_psk;
          if ($stream_psk == $psk) {
            $accepted = TRUE;
            $res["allowed"] = TRUE;

            // Redirect to main app when there is a matching alias.
            if (in_array($incoming_app, $service->getAliasesFromField($field))) {
              $app = $service->getAppFromEntity($entity);
              $res['new_url'] = strtolower($protocol) . '://' . $host . ':' . $port . '/' . $app . '/' . $incoming_stream . '?psk=' . $stream_psk;
              \Drupal::logger('ovenmedia')->notice('redirect %app/%stream to %new_url',
              [
                '%new_url' => $res['new_url'],
                '%app' => $app,
                '%stream' => $incoming_stream,
              ]);
            }
            $now = DrupalDateTime::createFromTimestamp(time());
            $now->setTimezone(new \DateTimeZone(\Drupal::config('system.date')->get('timezone')['default']));

            if ($status == 'opening') {
              $entity->{$field_name}->ome_active = 1;
              $entity->{$field_name}->ome_laststream = $now->getTimestamp();
               // Fire opening Event
              $event = new OvenMediaIncomingStreamAdmissionOpeningEvent($entity);
              $event_dispatcher = \Drupal::service('event_dispatcher');
              $event_dispatcher->dispatch($event, OvenMediaIncomingStreamAdmissionOpeningEvent::OPENING_EVENT);

            }
            if ($status == 'closing') {
              $entity->{$field_name}->ome_active = 0;
              $entity->{$field_name}->ome_laststream = $now->getTimestamp();
              // Fire closing Event
              $event = new OvenMediaIncomingStreamAdmissionClosingEvent($entity);
              $event_dispatcher = \Drupal::service('event_dispatcher');
              $event_dispatcher->dispatch($event, OvenMediaIncomingStreamAdmissionClosingEvent::CLOSING_EVENT);              
            }
          }

          $entity->save();

          \Drupal::logger('ovenmedia')->notice("%direction %protocol stream %status from %src:%client_port to %host\n %url",
          [
            '%direction' => $direction,
            '%src' => $src,
            '%host' => $host,
            '%client_port' => $client_port,
            '%url' => $url,
            '%protocol' => $protocol,
            '%stream_name' => $incoming_stream,
            '%status' => $status,
          ]);

        }
      }

    }

    $headers = [
      'Content-type' => 'application/vnd.api+json',
      'Accept' => 'application/vnd.api+json',
    ];
    if ($accepted) {
      return new JsonResponse($res);
    }
    else {
      throw new AccessDeniedHttpException("denied");
    }
  }

}
