<?php

namespace Drupal\ovenmedia\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\ovenmedia\OmeVhostInterface;

/**
 * Defines the ovenmediaengine vhost entity type.
 *
 * @ConfigEntityType(
 *   id = "ome_vhost",
 *   label = @Translation("OvenmediaEngine Vhost"),
 *   label_collection = @Translation("OvenmediaEngine Vhosts"),
 *   label_singular = @Translation("ovenmediaengine vhost"),
 *   label_plural = @Translation("ovenmediaengine vhosts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count ovenmediaengine vhost",
 *     plural = "@count ovenmediaengine vhosts",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\ovenmedia\OmeVhostListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ovenmedia\Form\OmeVhostForm",
 *       "edit" = "Drupal\ovenmedia\Form\OmeVhostForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "ome_vhost",
 *   admin_permission = "administer ome_vhost",
 *   links = {
 *     "collection" = "/admin/config/media/ome-vhost",
 *     "add-form" = "/admin/config/media/ome-vhost/add",
 *     "edit-form" = "/admin/config/media/ome-vhost/{ome_vhost}",
 *     "delete-form" = "/admin/config/media/ome-vhost/{ome_vhost}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "ovenmedia_origin",
 *     "ovenmedia_port_rtmp",
 *     "ovenmedia_port_srt",
 *     "ovenmedia_edge",
 *     "ovenmedia_edge_ssl",
 *     "ovenmedia_port_llhls",
 *     "ovenmedia_port_wss",
 *     "ovenmedia_api_url",
 *   }
 * )
 */
class OmeVhost extends ConfigEntityBase implements OmeVhostInterface {

  /**
   * The ovenmediaengine vhost ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The ovenmediaengine vhost label.
   *
   * @var string
   */
  protected $label;

  /**
   * The ovenmediaengine vhost status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The ovenmedia engine Origin Vhost.
   *
   * @var string
   */
  protected $ovenmedia_origin;

  /**
   * The ovenmedia engine Edge Hostname.
   *
   * @var string
   */
  protected $ovenmedia_edge;

  /**
   * Enable ovenmedia Signed Policy.
   *
   * @var bool
   */
  protected $ovenmedia_policy;

  /**
   * The ovenmedia engine Origin RTMP Port.
   *
   * @var int
   */
  protected $ovenmedia_port_rtmp;

  /**
   * The ovenmedia engine Origin SRT Port.
   *
   * @var int
   */
  protected $ovenmedia_port_srt;

  /**
   * The ovenmedia engine Origin LLHLS Port.
   *
   * @var int
   */
  protected $ovenmedia_port_llhls;

  /**
   * The ovenmedia engine Origin WSS Port.
   *
   * @var int
   */
  protected $ovenmedia_port_wss;

  /**
   * The ovenmedia engine Origin API Url.
   *
   * @var string
   */

  protected $ovenmedia_api_url;

  /**
   * Enable ovenmedia Edge SSL.
   *
   * @var bool
   */
  protected $ovenmedia_edge_ssl;

  /**
   * {@inheritdoc}
   */
  public function ovenmediaOrigin() {
    return $this->ovenmedia_origin;
  }

  /**
   * {@inheritdoc}
   */
  public function ovenmediaEdge() {
    return $this->ovenmedia_edge;
  }

  /**
   * {@inheritdoc}
   */
  public function ovenmediaPolicy() {
    return $this->ovenmedia_policy;
  }

  /**
   * {@inheritdoc}
   */
  public function ovenmediaPortRtmp() {
    return $this->ovenmedia_port_rtmp;
  }

  /**
   * {@inheritdoc}
   */
  public function ovenmediaPortSrt() {
    return $this->ovenmedia_port_srt;
  }

  /**
   * {@inheritdoc}
   */
  public function ovenmediaPortLlhls() {
    return $this->ovenmedia_port_llhls;
  }

  /**
   * {@inheritdoc}
   */
  public function ovenmediaPortWss() {
    return $this->ovenmedia_port_wss;
  }

  /**
   * {@inheritdoc}
   */
  public function ovenmediaApiUrl() {
    return $this->ovenmedia_api_url;
  }

  /**
   * {@inheritdoc}
   */
  public function ovenmediaEdgeSsl() {
    return $this->ovenmedia_edge_ssl;
  }

}
