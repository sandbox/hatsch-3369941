<?php

namespace Drupal\ovenmedia\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * OvenmediaEngine Vhost form.
 *
 * @property \Drupal\ovenmedia\OmeVhostInterface $entity
 */
class OmeVhostForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the ovenmediaengine vhost.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\ovenmedia\Entity\OmeVhost::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['ovenmedia_origin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ovenmedia Origin Server'),
      '#default_value' => $this->entity->ovenmediaOrigin(),
    ];
    $form['ovenmedia_policy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Make use of ovenmedias Signed Policy feature.'),
      '#description' => $this->t('set the key in settings.php'),
      '#default_value' => $this->entity->ovenmediaPolicy(),
    ];
    $form['ovenmedia_port_rtmp'] = [
      '#type' => 'number',
      '#title' => $this->t('Ovenmedia Origin RTMP Port'),
      '#default_value' => $this->entity->ovenmediaPortRtmp(),
    ];
    $form['ovenmedia_port_srt'] = [
      '#type' => 'number',
      '#title' => $this->t('Ovenmedia Origin SRT Port'),
      '#default_value' => $this->entity->ovenmediaPortSrt(),
    ];
    $form['ovenmedia_edge'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ovenmedia Edge Server'),
      '#default_value' => $this->entity->ovenmediaEdge(),
    ];
    $form['ovenmedia_edge_ssl'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('SSL for Edge'),
      '#default_value' => $this->entity->ovenmediaEdgeSsl(),
    ];
    $form['ovenmedia_port_llhls'] = [
      '#type' => 'number',
      '#title' => $this->t('Ovenmedia Edge LLHLS Port'),
      '#default_value' => $this->entity->ovenmediaPortLlhls(),
    ];
    $form['ovenmedia_port_wss'] = [
      '#type' => 'number',
      '#title' => $this->t('Ovenmedia Edge WEBRTC Port'),
      '#default_value' => $this->entity->ovenmediaPortWss(),
    ];
    $form['ovenmedia_api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ovenmedia Origin API Url'),
      '#description' => $this->t('set the key in settings.php'),
      '#default_value' => $this->entity->ovenmediaApiUrl(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new ovenmediaengine vhost %label.', $message_args)
      : $this->t('Updated ovenmediaengine vhost %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
