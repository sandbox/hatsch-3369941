<?php

namespace Drupal\ovenmedia\Plugin\Field\FieldFormatter;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'ovenmedia' formatter.
 *
 * @FieldFormatter(
 *   id = "ovenmedia_player",
 *   label = @Translation("OvenMedia Player"),
 *   field_types = {
 *     "ovenmedia"
 *   }
 * )
 */
class OvenMediaPlayerFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = new static(
    $plugin_id,
    $plugin_definition,
    $configuration['field_definition'],
    $configuration['settings'],
    $configuration['label'],
    $configuration['view_mode'],
    $configuration['third_party_settings'],
    );
    // Add any services you want to inject here.
    $plugin->ovenmedia = $container->get('ovenmedia.service');
    return $plugin;

  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {

      $entity = $item->getParent()->getParent()->getEntity();

      $llhls_url = $this->ovenmedia->getEdgeUrl($entity, 'llhls');
      $wss_url = $this->ovenmedia->getEdgeUrl($entity, 'wss');

      if ($item->ome_enabled == 1) {
        $element[$delta] = [
          '#theme' => 'ovenmedia_player_formatter',
          '#wss_url' => $wss_url,
          '#llhls_url' => $llhls_url,
          '#attached' => ['library' => ['ovenmedia/ovenplayer']],
        ];
      }
      else {
        $element[$delta] = [
          '#markup' => t('Complete your Streaming Profile to enable streaming.'),
        ];
      }
    }

    return $element;
  }

}
