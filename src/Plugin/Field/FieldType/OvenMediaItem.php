<?php

namespace Drupal\ovenmedia\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'ovenmedia' field type.
 *
 * @FieldType(
 *   id = "ovenmedia",
 *   label = @Translation("Ovenmedia"),
 *   category = @Translation("General"),
 *   default_widget = "ovenmedia_default",
 *   default_formatter = "ovenmedia_player",
 *   cardinality = 1
 * )
 *
 * @DCG
 * If you are implementing a single value field type you may want to inherit
 * this class form some of the field type classes provided by Drupal core.
 * Check out /core/lib/Drupal/Core/Field/Plugin/Field/FieldType directory for a
 * list of available field type implementations.
 */
class OvenMediaItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return "ovenmedia";
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = [
      'ome_app_name' => 'app',
      'ome_custom_stream_name' => 0,
      'ome_app_aliases' => '',
      'ome_custom_stream_psk' => 0,
      'ome_vhost' => '',
    ];

    return $settings + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $entity_storage = \Drupal::service('entity_type.manager')
      ->getStorage('ome_vhost');
    $config_entities = $entity_storage->loadMultiple();

    $vhost_values[0] = 'select';
    foreach ($config_entities as $config_entity) {
      $vhost_values[$config_entity->id()] = $config_entity->label();
    }

    // The values for the dropdown box.
    $vhost_options['#type_options'] = [
      '#type' => 'value',
      '#value' => $vhost_values,
    ];

    $element['ome_vhost'] = [
      '#type' => 'select',
      '#title' => $this->t('VHost'),
      // '#default_value' => $this->getSetting('ome_app_name'),
      '#options' => $vhost_values,
      '#default_value' => $this->getSetting('ome_vhost'),
      '#description' => $this->t('Set the App name for streams created with this field. Must be configured in OvenMediaEngine'),
      '#required' => TRUE,
    ];
    $element['ome_app_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Streaming App Name'),
      '#default_value' => $this->getSetting('ome_app_name'),
      '#description' => $this->t('Set the App name for streams created with this field. Must be configured in OvenMediaEngine'),
      '#required' => TRUE,
    ];

    $element['ome_app_aliases'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App aliases'),
      '#description' => $this->t('Comma seperated list of app aliases that will redirect incoming streams to the main app.'),
      '#default_value' => $this->getSetting('ome_app_aliases'),
    ];

    $element['ome_custom_stream_name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Custom Stream Name.'),
      '#default_value' => $this->getSetting('ome_custom_stream_name'),
      '#description' => $this->t('Wether or not a user can set a custom Stream Name'),
    ];

    $element['ome_custom_stream_psk'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Custom Password.'),
      '#default_value' => $this->getSetting('ome_custom_stream_psk'),
      '#description' => $this->t('Allow user to enter custom Password for the Stream.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $ome_vhost = $this->get('ome_vhost')->getValue();
    $ome_enabled = $this->get('ome_enabled')->getValue();
    $ome_stream_name = $this->get('ome_stream_name')->getValue();
    $ome_stream_psk = $this->get('ome_stream_psk')->getValue();
    $ome_active = $this->get('ome_active')->getValue();
    return empty($ome_enabled) && empty($ome_stream_name) && empty($ome_stream_psk) && empty($ome_active);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // @todo only add fields that are enabled in settings.
    $properties['ome_enabled'] = DataDefinition::create('boolean')
      ->setLabel(t('Streaming Enabled'))
      ->setDescription(t('Wether or not this Stream is enabled.'))
      ->setRequired(TRUE);
    $properties['ome_vhost'] = DataDefinition::create('string')
      ->setLabel(t('Stream Vhost'))
      ->setDescription(t('The Vhost that should be used for the Stream. Add them at <a href="/admin/config/media/ome-vhost">/admin/config/media/ome-vhost</a>'));
    $properties['ome_stream_name'] = DataDefinition::create('string')
      ->setLabel(t('Stream Name'))
      ->setDescription(t('The Name of the Stream.'));
    $properties['ome_stream_psk'] = DataDefinition::create('string')
      ->setLabel(t('Stream PSK'))
      ->setDescription(t('The Password for connecting to the Streaming Server with Url Query <i>?psk=</i>'));
    $properties['ome_active'] = DataDefinition::create('boolean')
      ->setLabel(t('Stream is active'))
      ->setDescription(t('Wether the Stream is active or not.'))
      ->setRequired(FALSE);
    $properties['ome_laststream'] = DataDefinition::create('string')
      ->setLabel(t('Last Stream'))
      ->setDescription(t('When was the last event for the stream. Can be Closing or Opening.'))
      ->setRequired(FALSE);
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    // @todo only add fields that are enabled in settings.
    return [
      'columns' => [
        'ome_enabled' => [
          'type' => 'int',
          'size' => 'tiny',
        ],
        'ome_active' => [
          'type' => 'int',
          'size' => 'tiny',
        ],
        'ome_stream_name' => [
          'type' => 'varchar',
          'length' => '10',
        ],
        'ome_stream_psk' => [
          'type' => 'varchar',
          'length' => '12',
        ],
        'ome_laststream' => [
          'type' => 'varchar',
          'length' => 20,
        ],

      ],
    ];
  }

}
