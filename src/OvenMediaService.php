<?php

namespace Drupal\ovenmedia;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Site\Settings;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Entity\ContentEntityType;

/**
 *
 */
class OvenMediaService {

  /**
   *
   */
  public function b64UrlEnc($str) {
    return str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($str));
  }

  /**
   *
   */
  public function signedPolicySignature($base_url) {
    $hmac_private_key = Settings::get('ovenmedia')['policy_key'];
    $signature = pack('H*', hash_hmac("sha1", $base_url, $hmac_private_key));
    return $this->b64UrlEnc($signature);

  }

  /**
   *
   */
  public function XOmeSignature($payload, $secret) {
    $signature = pack('H*', hash_hmac("sha1", $payload, $secret));
    return $this->b64UrlEnc($signature);

  }

  /**
   *
   */
  public function signedPolicy($policy) {
    return $this->b64UrlEnc($policy);
  }

  /**
   *
   */
  public function getUuid($entity) {
    return substr($entity->uuid(), 0, 6);
  }

  /**
   *
   */
  public function getAdmissionWebhookSecret() {
    // $settings = Settings::get('ovenmedia.settings');
    return Settings::get('ovenmedia')['x_ome_header_secret'];
  }

  /**
   *
   */
  public function getStreamUrl(EntityInterface $entity, $protocol) {
    $config = $this->getVhostConfigFromEntity($entity);
    // $config = \Drupal::config('ovenmedia.settings');
    $server = $config->get('ovenmedia_origin');
    $server_srt_port = $config->get('ovenmedia_port_srt');
    $server_rtmp_port = $config->get('ovenmedia_port_rtmp');
    $app = $this->getAppFromEntity($entity);
    // $field_values = $entity->get($field_name)->first()->getValue();
    $streamid = $this->getStreamFromEntity($entity);
    $psk = $this->getPskFromEntity($entity);
    $base_url = $protocol . '://' . $server . ':';

    // $hmac_string = $base_url . "?" . $policy_query_key_name . "=" . $base64_encoded_policy;
    // $final_url = $base_url . "?" . $policy_query_key_name . "=" . $base64_encoded_policy . "&" . $signature_query_key_name . "=" . $base64_encoded_signature;
    $policy = "{\"url_expire\":7245594130000}";
    switch ($protocol) {
      case 'rtmp':
        $stream_url = $base_url . $server_rtmp_port . '/' . $app . '/' . $streamid . '?psk=' . $psk;

        if ($config->get('ovenmedia_policy')) {
          $stream_url_policy = $stream_url . '&policy=' . $this->signedPolicy($policy);
          $stream_url = $stream_url_policy . '&signature=' . $this->signedPolicySignature($stream_url_policy);
        }
        break;

      case 'srt':
        $srt_streamid = $base_url . $server_srt_port . '/' . $app . '/' . $streamid . '?psk=' . $psk;
        $stream_url = $base_url . $server_srt_port . '?streamid=' . urlencode($srt_streamid);
        if ($config->get('ovenmedia_policy')) {
          $srt_streamid_policy = $base_url . $server_srt_port . '/' . $app . '/' . $streamid . '&policy=' . $this->signedPolicy($policy);
          $stream_url = $base_url . $server_srt_port . '?streamid=' . urlencode($srt_streamid_policy . '&signature=' . $this->signedPolicySignature($srt_streamid_policy));
        }
        break;

    }
    return $stream_url;
  }

  /**
   *
   */
  public function getEdgeUrl(EntityInterface $entity, $protocol) {
    $config = $this->getVhostConfigFromEntity($entity);
    // $config = \Drupal::config('ovenmedia.settings');
    $server = $config->get('ovenmedia_edge');
    $ssl = $config->get('ovenmedia_edge_ssl');

    $app = $this->getAppFromEntity($entity);
    // $field_values = $entity->get($field_name)->first()->getValue();
    $streamid = $this->getStreamFromEntity($entity);
    $base_url = $protocol . '://' . $server . ':';

    // $hmac_string = $base_url . "?" . $policy_query_key_name . "=" . $base64_encoded_policy;
    // $final_url = $base_url . "?" . $policy_query_key_name . "=" . $base64_encoded_policy . "&" . $signature_query_key_name . "=" . $base64_encoded_signature;
    $policy = "{\"url_expire\":7245594130000}";
    switch ($protocol) {
      case 'wss':
        $server_wss_port = $config->get('ovenmedia_port_wss');
        $scheme = 'ws://';
        if ($ssl) {
          $scheme = 'wss://';
        }
        $base_url = $scheme . $server . ':' . $server_wss_port;
        // @todo substiture { app } and {stream } from config.
        $edge_url = $base_url . '/' . $app . '/' . $streamid . '/webrtc';
        // @todo .
        /*
        if ($config->get('ovenmedia_policy')) {
        $stream_url_policy = $stream_url.'&policy='.$this->signedPolicy($policy);
        $stream_url = $stream_url_policy.'&signature='.$this->signedPolicySignature($stream_url_policy);
        }*/
        break;

      case 'llhls':
        // @todo substiture { app } and {stream } from config.
        $server_llhls_port = $config->get('ovenmedia_port_llhls');
        $scheme = 'http://';
        if ($ssl) {
          $scheme = 'https://';
        }
        $base_url = $scheme . $server . ':' . $server_llhls_port;
        $edge_url = $base_url . '/' . $app . '/' . $streamid . '/llhls.m3u8';
        // @todo .
        /*
        if ($config->get('ovenmedia_policy')) {
        $srt_streamid_policy = $base_url.$server_srt_port.'/'.$app.'/'.$streamid.'&policy='.$this->signedPolicy($policy);
        $stream_url = $base_url.$server_srt_port.'?streamid='.urlencode($srt_streamid_policy.'&signature='.$this->signedPolicySignature($srt_streamid_policy));
        }
         */
        break;

    }
    return $edge_url;
  }

  /**
   *
   */
  public function getStreamFromEntity(EntityInterface $entity) {
    $fields = $entity->getFieldDefinitions();
    foreach ($fields as $field) {
      if ($field->getType() == 'ovenmedia') {
        $field_name = $field->getName();
        $values = $entity->get($field_name)->first()->getValue();
        return $values['ome_stream_name'];
      }
    }
  }

  /**
   *
   */
  public function getPskFromEntity(EntityInterface $entity) {
    $fields = $entity->getFieldDefinitions();
    foreach ($fields as $field) {
      if ($field->getType() == 'ovenmedia') {
        $field_name = $field->getName();
        $values = $entity->get($field_name)->first()->getValue();
        return $values['ome_stream_psk'];
      }
    }
  }

  /**
   *
   */
  public function getAliasesFromField($field) {
    $aliases = $field->getSettings()['ome_app_aliases'];
    return explode(',', str_replace(' ', '', $aliases));
  }

  /**
   *
   */
  public function getConfigIdFromHost($host) {
    $config = \Drupal::service('entity_type.manager')
      ->getStorage('ome_vhost')
      ->loadByProperties(['ovenmedia_origin' => $host]);
    return array_shift($config)->id();
  }

  /**
   *
   */
  public function getFieldFromAppName($incoming_app) {
    $entity_types = \Drupal::entityTypeManager()->getDefinitions();
    foreach ($entity_types as $entity_type => $entity_type_definition) {
      if ($entity_type_definition instanceof ContentEntityType) {
        $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type);
        foreach ($bundles as $bundle => $item) {
          $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, $bundle);
          foreach ($fields as $field) {
            if ($field->getType() == 'ovenmedia') {

              $app = $field->getSettings()['ome_app_name'];
              $app_aliases = $this->getAliasesFromField($field);

              // Check if incoming app matches the app configured in the field instance.
              if ($incoming_app == $app || in_array($incoming_app, $app_aliases)) {
                return [
                  'entity_type' => $entity_type,
                  'field' => $field,
                ];
              }
            }
          }
        }
      }
    }
  }

  /**
   *
   */
  public function getAppFromEntity(EntityInterface $entity) {
    $ovenmedia_fields = \Drupal::service('entity_type.manager')->getStorage('field_storage_config')->loadByProperties(
      [
        'type' => 'ovenmedia',
      ]);
    $bundle = $entity->bundle();
    $entity_type = $entity->getEntityType();
    foreach ($ovenmedia_fields as $field) {
      $field_storage = FieldStorageConfig::loadByName($entity_type->id(), $field->getName());
      if (!empty($field_storage) && in_array($bundle, $field_storage->getBundles())) {
        $definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type->id(), $bundle);
        return $definitions[$field->getName()]->getSetting('ome_app_name');
      }
    }
  }

  /**
   *
   */
  public function getVhostConfigFromEntity(EntityInterface $entity) {
    $ovenmedia_fields = \Drupal::service('entity_type.manager')->getStorage('field_storage_config')->loadByProperties(
      [
        'type' => 'ovenmedia',
      ]);
    $bundle = $entity->bundle();
    $entity_type = $entity->getEntityType();
    foreach ($ovenmedia_fields as $field) {

      $field_storage = FieldStorageConfig::loadByName($entity_type->id(), $field->getName());
      if (!empty($field_storage) && in_array($bundle, $field_storage->getBundles())) {
        $definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type->id(), $bundle);
        $config_id = $definitions[$field->getName()]->getSetting('ome_vhost');

        $config = \Drupal::service('entity_type.manager')
          ->getStorage('ome_vhost')
          ->loadByProperties(['id' => $config_id]);

        return array_shift($config);

      }
    }
  }

}
