<?php

/**
 * @file
 * Make ovenmedia fields available to views.
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_views_data().
 */
function ovenmedia_field_views_data(FieldStorageConfigInterface $field) {
  $data = views_field_default_views_data($field);
  $field_type = $field->getType();
  $field_name = $field->getName();
  $settings = $field->getSettings();

  if ($field_type == 'ovenmedia') {
    $columns = [
      'ome_enabled' => 'boolean',
      'ome_stream_name' => 'standard',
      'ome_active' => 'boolean',
      'ome_laststream' => 'date',
    ];
    foreach ($data as $table_name => $table_data) {
      foreach ($columns as $column => $plugin_id) {
        $data[$table_name][$field_name . '_' . $column]['field'] = [
          'id' => $plugin_id,
          'field_name' => $field_name,
          'property' => $column,
        ];

      }

      // Add values from field settings to views.
      $data[$table_name][$field_name . '_ome_app'] = [
        'title' => $field_name,
        'group' => $table_name,
        'title_short' => 'APP',
        'field' => [
          'title' => t('Ovenmedia App'),
          'group' => $table_name,
          'help' => t('Ovenmedia APP Name'),
          'id' => 'ovenmedia_app',
        ],
      ];
    }
  }

  return $data;
}
